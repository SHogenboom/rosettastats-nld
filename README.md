# Nieuw Hoofdstuk Toevoegen

1. `R Studio`: `File` > `New File` > `Quarto Document...` > `Empty Document` (links onder)
1. Begin het document met de `YAML` header:

```{}
#| eval = FALSE
---
title: "Titel van het Hoofdstuk"
meta-description: |
  "Een korte beschrijving van het hoofdstuk. Let op: maak de beschrijving informatief voor studenten! De beschrijving wordt automatisch getoond in de Inhoudsopgave."
---
```

1. Voeg onder de `yaml` header de titel van het hoofdstuk toe & een referentie naar dit hoofdstuk:
```{}
# Titel van het Hoofdstuk {#titel-van-het-hoofdstuk}
```

1. Sla het document op in de `chapters/` map. Geef het document een informatieve naam & het nummer van het hoofdstuk in het boek. Op het moment bepaald de titel volgorde - door de nummers - namelijk de plaatsing van hoofdstukken in de Inhoudsopgave. Wanneer je hoofdstuk tussen bestaande hoofdstukken valt zul je die hoofdstukken dus ook opnieuw moeten nummeren.

1. Voeg de gewenste inhoud toe aan het document. De code hieronder kun je gebruiken als template voor het toevoegen van code/output voorbeelden per software:

```{md}
#| eval = FALSE
::: {.panel-tabset group="software"}

## R

Tekst, code, en output die van toepassing is op R / R Studio

## Jamovi

Tekst, code, en output die van toepassing is op Jamovi

## SPSS

Tekst, code, en output die van toepassing is op SPSS
:::

```

1. Voeg het hoofdstuk toe aan het boek door het bestand `_quarto.yml` te updaten.
Je plaatst de naam van het bestand `chapters/naam-van-bestand.qmd` toe aan de content sectie op de gewenste plek.

# Lokaal Boek Samenstellen

Om het boek lokaal te kunnen bekijken - en te zien of je aanpassingen het gewenste effect hebben - gebruik je het commando: `quarto::quarto_preview()`.

Dit zorgt ervoor dat het boek samengesteld wordt en getoond in de `Viewer`.
